import React, { Component } from 'react';

import { View, Text, Image } from 'react-native';
import { TextInput, ScrollView } from 'react-native-gesture-handler';

// import { Container } from './styles';

export default class Buscar extends Component {
  render() {
    return(
        <View style={{ width: '100%', alignItems: 'center' }}>
          <View
            style={{ backgroundColor: '#922828', width: '100%', 
                    paddingVertical: 10, justifyContent: 'center', 
                    alignItems: 'center' }}
          >
            <Text style={{ fontSize: 24, color: 'white' }} > Buscar </Text>
          </View>

          <View style={{ width: '100%', marginVertical: 10, paddingHorizontal: 10 }}>
            <TextInput 
              style={{ 
                backgroundColor: '#E4E4E4',
                borderWidth: .5,
                borderColor: 'grey',
                borderRadius: 5,
                fontSize: 18
               }}
              placeholder='Digite aqui...' />
          </View>

          <ScrollView
            style={{ 
              maxHeight: '82%'
            }}
          >
            <View style={{ width: '100%', marginVertical: 10, 
                          paddingHorizontal: 10 }}>
                    <View style={{ backgroundColor: '#E7E7E7', width: 340,
                                    height: 130, 
                                      paddingVertical: 10, 
                                      flexDirection: 'row', 
                                      justifyContent: 'space-around',
                                      alignContent: 'flex-start',
                                      borderRadius: 5,
                                      marginVertical: 5  }}>
                        <Image
                          style={{ width: 150, height: 110, borderRadius: 5 }}
                          source={require('../assets/images/marmita.jpg')} />

                        <View
                          style={{ flexDirection: 'column', 
                                  justifyContent: 'space-between' }}
                        >
                          <Text>
                            Breve descrição sobre o{'\n'}que vem dentro{'\n'}da marmita.
                          </Text>

                          <Text style={{ paddingLeft: 100, fontSize: 18 }} > R$0,00 </Text>
                        </View>
                      </View>

                      <View style={{ backgroundColor: '#E7E7E7', width: 340,
                                    height: 130, 
                                      paddingVertical: 10, 
                                      flexDirection: 'row', 
                                      justifyContent: 'space-around',
                                      alignContent: 'flex-start',
                                      borderRadius: 5,
                                      marginVertical: 5  }}>
                        <Image
                          style={{ width: 150, height: 110, borderRadius: 5 }}
                          source={require('../assets/images/marmita.jpg')} />

                        <View
                          style={{ flexDirection: 'column', 
                                  justifyContent: 'space-between' }}
                        >
                          <Text>
                            Breve descrição sobre o{'\n'}que vem dentro{'\n'}da marmita.
                          </Text>

                          <Text style={{ paddingLeft: 100, fontSize: 18 }} > R$0,00 </Text>
                        </View>
                      </View>

                      <View style={{ backgroundColor: '#E7E7E7', width: 340,
                                    height: 130, 
                                      paddingVertical: 10, 
                                      flexDirection: 'row', 
                                      justifyContent: 'space-around',
                                      alignContent: 'flex-start',
                                      borderRadius: 5,
                                      marginVertical: 5 }}>
                        <Image
                          style={{ width: 150, height: 110, borderRadius: 5 }}
                          source={require('../assets/images/marmita.jpg')} />

                        <View
                          style={{ flexDirection: 'column', 
                                  justifyContent: 'space-between' }}
                        >
                          <Text>
                            Breve descrição sobre o{'\n'}que vem dentro{'\n'}da marmita.
                          </Text>

                          <Text style={{ paddingLeft: 100, fontSize: 18 }} > R$0,00 </Text>
                        </View>
                      </View>


                      <View style={{ backgroundColor: '#E7E7E7', width: 340,
                                    height: 130, 
                                      paddingVertical: 10, 
                                      flexDirection: 'row', 
                                      justifyContent: 'space-around',
                                      alignContent: 'flex-start',
                                      borderRadius: 5,
                                      marginVertical: 5 }}>
                        <Image
                          style={{ width: 150, height: 110, borderRadius: 5 }}
                          source={require('../assets/images/marmita.jpg')} />

                        <View
                          style={{ flexDirection: 'column', 
                                  justifyContent: 'space-between' }}
                        >
                          <Text>
                            Breve descrição sobre o{'\n'}que vem dentro{'\n'}da marmita.
                          </Text>

                          <Text style={{ paddingLeft: 100, fontSize: 18 }} > R$0,00 </Text>
                        </View>
                      </View>


                      <View style={{ backgroundColor: '#E7E7E7', width: 340,
                                    height: 130, 
                                      paddingVertical: 10, 
                                      flexDirection: 'row', 
                                      justifyContent: 'space-around',
                                      alignContent: 'flex-start',
                                      borderRadius: 5,
                                      marginVertical: 5 }}>
                        <Image
                          style={{ width: 150, height: 110, borderRadius: 5 }}
                          source={require('../assets/images/marmita.jpg')} />

                        <View
                          style={{ flexDirection: 'column', 
                                  justifyContent: 'space-between' }}
                        >
                          <Text>
                            Breve descrição sobre o{'\n'}que vem dentro{'\n'}da marmita.
                          </Text>

                          <Text style={{ paddingLeft: 100, fontSize: 18 }} > R$0,00 </Text>
                        </View>
                      </View>
            </View>
          </ScrollView>
        </View>
    );
  }
}
