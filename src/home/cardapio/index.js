import { createAppContainer } from 'react-navigation'

import { createStackNavigator } from 'react-navigation-stack'

import Cardapio from './cardapio'

const StackCardapio = createAppContainer(
    createStackNavigator({
        Cardapio: {
            screen: Cardapio,
            navigationOptions: {
                headerShown: false
            }
        },
        Finalizar: {
            screen: Cardapio,
            navigationOptions: {
                headerShown: true
            }
        }
    })
);

export default StackCardapio;