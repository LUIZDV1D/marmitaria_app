import React, { Component } from 'react';

import { View, Text, Image, ScrollView, TouchableOpacity } from 'react-native';

// import { Container } from './styles';

export default class Cardapio extends Component {
  render() {
    return(
        <View style={{ width: '100%', alignItems: 'center' }}>
          <View
            style={{ backgroundColor: '#922828', width: '100%', 
                    paddingVertical: 10, justifyContent: 'center', 
                    alignItems: 'center' }}
          >
            <Text style={{ fontSize: 24, color: 'white' }} > Cardápio </Text>
          </View>

          <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ fontSize: 20 }} > Marmitas </Text>
          </View>

          <View style={{ width: '100%', justifyContent: 'space-between', 
                          alignContent: 'center', flexDirection: 'row' }}>
            <Text style={{ fontSize: 15, color: '#6C6C6C' }} > Do dia </Text>
            <TouchableOpacity>
              <Text style={{ fontSize: 15, color: '#922828' }} > Ver todos </Text>
            </TouchableOpacity>
          </View>

          <ScrollView
              horizontal
              showsHorizontalScrollIndicator={false}
              style={{ width: '100%' }}
          >
              <View
                style={{ marginVertical: 10, 
                        flexDirection: 'row' }}
              >
                <View style={{ backgroundColor: '#E7E7E7', width: 340,
                              height: 130, 
                                paddingVertical: 10, 
                                flexDirection: 'row', 
                                justifyContent: 'space-around',
                                alignContent: 'flex-start',
                                borderRadius: 5,
                                marginHorizontal: 5  }}>
                  <Image
                    style={{ width: 150, height: 110, borderRadius: 5 }}
                    source={require('../../assets/images/marmita.jpg')} />

                  <View
                    style={{ flexDirection: 'column', 
                            justifyContent: 'space-between' }}
                  >
                    <Text>
                      Breve descrição sobre o{'\n'}que vem dentro{'\n'}da marmita.
                    </Text>

                    <Text style={{ paddingLeft: 100, fontSize: 18 }} > R$0,00 </Text>
                  </View>
                </View>


                <View style={{ backgroundColor: '#E7E7E7', width: 340,
                              height: 130, 
                                paddingVertical: 10, 
                                flexDirection: 'row', 
                                justifyContent: 'space-around',
                                alignContent: 'flex-start',
                                borderRadius: 5,
                                marginHorizontal: 5 }}>
                  <Image
                    style={{ width: 150, height: 110, borderRadius: 5 }}
                    source={require('../../assets/images/marmita.jpg')} />

                  <View
                    style={{ flexDirection: 'column', 
                            justifyContent: 'space-between' }}
                  >
                    <Text>
                      Breve descrição sobre o{'\n'}que vem dentro{'\n'}da marmita.
                    </Text>

                    <Text style={{ paddingLeft: 100, fontSize: 18 }} > R$0,00 </Text>
                  </View>
                </View>


                <View style={{ backgroundColor: '#E7E7E7', width: 340,
                              height: 130, 
                                paddingVertical: 10, 
                                flexDirection: 'row', 
                                justifyContent: 'space-around',
                                alignContent: 'flex-start',
                                borderRadius: 5,
                                marginHorizontal: 5 }}>
                  <Image
                    style={{ width: 150, height: 110, borderRadius: 5 }}
                    source={require('../../assets/images/marmita.jpg')} />

                  <View
                    style={{ flexDirection: 'column', 
                            justifyContent: 'space-between' }}
                  >
                    <Text>
                      Breve descrição sobre o{'\n'}que vem dentro{'\n'}da marmita.
                    </Text>

                    <Text style={{ paddingLeft: 100, fontSize: 18 }} > R$0,00 </Text>
                  </View>
                </View>
              </View>
            </ScrollView>

          <View
                style={{ flexDirection: 'row' }}
              >
                <View style={{ width: '100%', flexDirection: 'row', 
                              justifyContent: 'space-around', alignItems: 'center' }}>
                  <View
                    style={{ width: '45%', justifyContent: 'center', alignItems: 'center' }}
                  >
                    <Text style={{ fontSize: 20 }} > Salgados </Text>
                    <View style={{ backgroundColor: '#E7E7E7',
                                    height: 140, 
                                    width: '100%',
                                    paddingVertical: 10, 
                                    paddingHorizontal: 10,
                                    flexDirection: 'column', 
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    borderRadius: 5,
                                    marginHorizontal: 5  }}>
                      <Image
                        style={{ width: '100%', height: '100%', borderRadius: 5 }}
                        source={require('../../assets/images/marmita.jpg')} />
                    </View>
                  </View>

                  <View
                    style={{ width: '45%', justifyContent: 'center', alignItems: 'center' }}
                  >
                    <Text style={{ fontSize: 20 }} > Doces </Text>
                    <View style={{ backgroundColor: '#E7E7E7',
                                    height: 140, 
                                    width: '100%',
                                    paddingVertical: 10, 
                                    paddingHorizontal: 10,
                                    flexDirection: 'column', 
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    borderRadius: 5,
                                    marginHorizontal: 5  }}>
                      <Image
                        style={{ width: '100%', height: '100%', borderRadius: 5 }}
                        source={require('../../assets/images/marmita.jpg')} />
                    </View>
                  </View>
                </View>
              </View>


              <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ fontSize: 20 }} > Mais vendidos </Text>
          </View>

          <View style={{ width: '100%', justifyContent: 'space-between', 
                          alignContent: 'center', flexDirection: 'row' }}>
            <Text style={{ fontSize: 15, color: '#6C6C6C' }} > Do dia </Text>
            <TouchableOpacity>
              <Text style={{ fontSize: 15, color: '#922828' }} > Ver todos </Text>
            </TouchableOpacity>
          </View>

          <ScrollView
              horizontal
              showsHorizontalScrollIndicator={false}
              style={{ width: '100%' }}
          >
              <View
                style={{ marginVertical: 10, 
                        flexDirection: 'row' }}
              >
                <View style={{ backgroundColor: '#E7E7E7', paddingVertical: 10, 
                                flexDirection: 'column', 
                                borderRadius: 5,
                                marginHorizontal: 5,
                                justifyContent: 'center',
                                alignItems: 'center'  }}>
                  <Image
                    style={{ width: 110, height: 90, borderRadius: 5 }}
                    source={require('../../assets/images/marmita.jpg')} />

                  <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Text> Nome do produto </Text>
                    <Text> R$0,00 </Text>
                  </View>
                </View>


                <View style={{ backgroundColor: '#E7E7E7', paddingVertical: 10, 
                                flexDirection: 'column', 
                                borderRadius: 5,
                                marginHorizontal: 5,
                                justifyContent: 'center',
                                alignItems: 'center'  }}>
                  <Image
                    style={{ width: 110, height: 90, borderRadius: 5 }}
                    source={require('../../assets/images/marmita.jpg')} />

                  <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Text> Nome do produto </Text>
                    <Text> R$0,00 </Text>
                  </View>
                </View>


                <View style={{ backgroundColor: '#E7E7E7', paddingVertical: 10, 
                                flexDirection: 'column', 
                                borderRadius: 5,
                                marginHorizontal: 5,
                                justifyContent: 'center',
                                alignItems: 'center'  }}>
                  <Image
                    style={{ width: 110, height: 90, borderRadius: 5 }}
                    source={require('../../assets/images/marmita.jpg')} />

                  <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Text> Nome do produto </Text>
                    <Text> R$0,00 </Text>
                  </View>
                </View>


                <View style={{ backgroundColor: '#E7E7E7', paddingVertical: 10, 
                                flexDirection: 'column', 
                                borderRadius: 5,
                                marginHorizontal: 5,
                                justifyContent: 'center',
                                alignItems: 'center'  }}>
                  <Image
                    style={{ width: 110, height: 90, borderRadius: 5 }}
                    source={require('../../assets/images/marmita.jpg')} />

                  <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Text> Nome do produto </Text>
                    <Text> R$0,00 </Text>
                  </View>
                </View>
              </View>
            </ScrollView>

        </View>
    );
  }
}
