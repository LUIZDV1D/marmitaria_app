import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs'

import StackCardapio from './cardapio'
import Buscar from './buscar'
import Pedidos from './pedidos'

const HomeBottom = createMaterialBottomTabNavigator({
    Cardapio: {
        screen: StackCardapio,
        navigationOptions: {
            title: 'Cardápio'
        }
    },
    Buscar: {
        screen: Buscar,
        navigationOptions: {
            title: 'Buscar'
        }
    },
    Pedidos: {
        screen: Pedidos,
        navigationOptions: {
            title: 'Pedidos'
        }
    }
},
{
    inactiveColor: '#9c9c9c',
    barStyle: { backgroundColor: '#922828' }
});

export default HomeBottom;