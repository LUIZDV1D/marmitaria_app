import React from 'react'

import { createAppContainer, createSwitchNavigator } from 'react-navigation'

import HomeBottom from './home'


const Routes = createAppContainer(
    createSwitchNavigator({
        HomeBottom
    })
);

export default Routes;